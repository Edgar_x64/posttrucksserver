### Run application
```
mvn spring-boot:run
```

### Database (MySQL)
```
The test dump of the database is in the folder 'resources'
SQL command: CREATE DATABASE postTrucks;
After in the terminal: mysql < dump.sql
```