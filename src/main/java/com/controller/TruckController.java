package com.controller;

import com.bean.TruckBean;
import com.service.TruckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/trucks")
@CrossOrigin("http://localhost:8081")
public class TruckController {

    private final TruckService truckService;

    @Autowired
    public TruckController(TruckService truckService) {
        this.truckService = truckService;
    }

    @GetMapping("/all")
    public List<TruckBean> all() {
        return truckService.getAll();
    }

    @PostMapping("/save")
    public void save(@RequestBody TruckBean truckBean) {
        truckService.save(truckBean);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        truckService.delete(id);
    }

}

