package com.configuration;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.Type;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class MapperConfiguration {

    @Bean
    public MapperFacade mapperFacade() {
        final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        mapperFactory.getConverterFactory().registerConverter(new CustomConverter<String, LocalDate>() {
            @Override
            public LocalDate convert(String s, Type<? extends LocalDate> type, MappingContext mappingContext) {
                return LocalDate.parse(s, formatter);
            }
        });
        mapperFactory.getConverterFactory().registerConverter(new CustomConverter<LocalDate, String>() {
            @Override
            public String convert(LocalDate localDate, Type<? extends String> type, MappingContext mappingContext) {
                return formatter.format(localDate);
            }
        });

        mapperFactory.getConverterFactory().registerConverter(new CustomConverter<String, LocalDateTime>() {
            @Override
            public LocalDateTime convert(String s, Type<? extends LocalDateTime> type, MappingContext mappingContext) {
                return LocalDateTime.parse(s, formatter);
            }
        });
        mapperFactory.getConverterFactory().registerConverter(new CustomConverter<LocalDateTime, String>() {
            @Override
            public String convert(LocalDateTime localDate, Type<? extends String> type, MappingContext mappingContext) {
                return formatter.format(localDate);
            }
        });

        return mapperFactory.getMapperFacade();
    }

}
