package com.service;

import com.bean.TruckBean;
import com.etity.Truck;
import com.repository.TruckRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TruckService {

    private final TruckRepository truckRepository;
    private final MapperFacade mapperFacade;

    @Autowired
    public TruckService(TruckRepository truckRepository, MapperFacade mapperFacade) {
        this.truckRepository = truckRepository;
        this.mapperFacade = mapperFacade;
    }

    public List<TruckBean> getAll() {
        List<Truck> trucks = truckRepository.findAll();
        return mapperFacade.mapAsList(trucks, TruckBean.class);
    }

    public void save(TruckBean truckBean) {
        Truck truck = mapperFacade.map(truckBean, Truck.class);
        truckRepository.save(truck);
    }

    public void delete(Long id) {
        truckRepository.delete(id);
    }
}
